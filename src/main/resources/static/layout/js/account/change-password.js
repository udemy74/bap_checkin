$(document).ready(function () {
    $("#changePasswordButton").click(function () {
        var url = "/account/changePassword";
        $.ajax({
            url: url,
            type: 'POST',
            contentType: "application/json",
            dataType: "json",
            headers: {
                "Authorization" : getAuthorization()
            },
            data: getPassword(),
        }).done(function (data) {
            if (data !== null) {
                message(data.messageType, data.message)
                $("#oldPassword").val("");
                $("#newPassword").val("");
            }
        });
    });
});

function getPassword() {
    var userInfo = {
        oldPassword : $("#oldPassword").val() ,
        newPassword : $("#newPassword").val()
    }
    return JSON.stringify(userInfo);
}