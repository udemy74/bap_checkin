const edit = "Edit Info"
const update = "Update";
var isUpdateButton = false;

$(document).ready(function () {
    var DATE_FORMAT = 'yy-mm-dd';
    $("#birthday").datepicker(
        {
            dateFormat: DATE_FORMAT,
            todayHighlight: true,
            defaultDate: '1994-01-01',
            autoclose: true
        }
    );
    getUserInfo();
    changeButton(false);

    $("#infoButton").click(function () {
        if (isUpdateButton) {
            updateInfo();
            isUpdateButton = false;
        }
        else {
            isUpdateButton = true;
        }
        changeButton(isUpdateButton);
    });
});

function getUserInfo() {
    var url = "/account";
    $.ajax({
        url: url,
        type: 'POST',
        contentType: "application/json",
        headers: {
            "Authorization" : getAuthorization()
        },
        dataType: "json"
    }).done(function (data) {
        if (data !== null) {
            setAccountInfo(data);
        }
    });
}

function updateInfo() {
    var url = "/account/update";
    $.ajax({
        url: url,
        type: 'POST',
        contentType: "application/json",
        dataType: "json",
        headers: {
            "Authorization" : getAuthorization()
        },
        data: getAccountInfo(),
    }).done(function (data) {
        if (data !== null) {
            message(data.messageType, data.message)
        }
    });
}

function setAccountInfo(user) {
    $("#userName").val(user.userName);
    $("#name").val(user.name);
    $("#birthday").val(user.birthday);
    $("#phoneNumber").val(user.phoneNumber);
    $("#email").val(user.email);
}

function getAccountInfo() {
    var userInfo = {
        userName : $("#userName").val() ,
        name : $("#name").val() ,
        birthday : $("#birthday").val(),
        phoneNumber : $("#phoneNumber").val()
    }
    return JSON.stringify(userInfo);
}

function changeButton(buttonStatus) {
    var button = $("#infoButton");
    var input = $("input");
    if (buttonStatus) {
        button.html(update);
        input.attr("disabled", false);
    }
    else {
        button.html(edit);
        input.attr("disabled", true);
    }

    $("#email").attr("disabled", true);
}