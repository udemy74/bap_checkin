$(document).ready(function () {
    getUserInfo();

    $("#checkinButton").click(function () {
        var url = "/checkinRequest";
        $.ajax({
            url: url,
            type: 'POST',
            contentType: "application/json",
            headers: {
                "Authorization" : getAuthorization()
            },
            dataType: "json"
        }).done(function (data) {
            if (data !== null) {
                $setChecinTime(data);
                var msg = data.messageResponse;
                message(msg.messageType, msg.message)
            }
        });
    });
});

function getUserInfo() {
    var url = "/checkinRequest";
    $.ajax({
        url: url,
        type: 'GET',
        contentType: "application/json",
        headers: {
            "Authorization" : getAuthorization()
        },
        dataType: "json"
    }).done(function (data) {
        if (data !== null) {
            setChecinTime(data);
        }
    });
}

function setChecinTime(checkinResponse) {
    $("#dateCheckin").text(checkinResponse.checkinDate);
    $("#checkinTime").text(checkinResponse.checkinTime);
    $("#checkoutTime").text(checkinResponse.checkoutTime);
}