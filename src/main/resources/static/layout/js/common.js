const option = {
    timeOut: 5000
}
const authorization = "Authorization"

$(document).ready(function () {
    setUserName();
});

function message(msgType, msg) {
    switch (msgType) {
        case "success":
            toastr.success(msg, "", option);
            break;
        case "error":
            toastr.error(msg, "", option);
    }
}

function getAuthorizationValue(request) {
    var authorizationValue = request.getResponseHeader(authorization)
    setAuthorization(authorizationValue);
}

function setAuthorization(authorizationValue) {
    Cookies.set(authorization, authorizationValue);
}

function getAuthorization() {
    return Cookies.get(authorization);
}

function setUserName() {
    var url = "/name";
    $.ajax({
        url: url,
        type: 'POST',
        contentType: "application/json",
        dataType: "json",
        headers: {
            "Authorization" : getAuthorization()
        }
    }).done(function (data) {
        if (data !== null) {
            $("#headerUserName").html(data.email);
        }
    });
}
