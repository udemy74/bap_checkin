package com.bap.checkin.ui.control.admin;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

@Controller
@PreAuthorize("hasAnyAuthority('Admin')")
public class AdminController {
}
