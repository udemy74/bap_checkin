package com.bap.checkin.ui.control.login;

import com.bap.checkin.security.SecurityConstants;
import com.bap.checkin.ui.common.request.AccountRequest;
import com.bap.checkin.ui.common.request.UserRequest;
import com.bap.checkin.ui.common.response.UserResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;


@Controller
@AllArgsConstructor
public class LoginController {

    private final LoginService loginService;

    /**
     * URL
     */
    private static final String LOGIN_FAIL_URL = "/login-error";
    private static final String HOME_URL = "/checkin";

    /**
     * Template
     */
    private static final String LOGIN_TEMPLATE = "login";
    private static final String HOME_TEMPLATE = "home";
    private static final String REGISTER_TEMPLATE = "register";

    @GetMapping(value = SecurityConstants.LOGIN_PAGE_URL)
    public String login(Authentication authentication,
                        Model model) {
        if (authentication != null && authentication.isAuthenticated()) {
            return "redirect:" + HOME_URL;
        }
        model.addAttribute("userRequest", new UserRequest());
        return LOGIN_TEMPLATE;
    }

    @GetMapping(value = LOGIN_FAIL_URL)
    public String login_fail(Authentication authentication,
                             Model model) {
        model.addAttribute("msg", "Login Fail!");
        return LOGIN_TEMPLATE;
    }

    @GetMapping(value = HOME_URL)
    public String home(Model model, Principal principal) {
        return HOME_TEMPLATE;
    }

    @GetMapping(value = SecurityConstants.REGISTER_URL)
    public String register(Authentication authentication,
                           Model model) {
        if (authentication != null && authentication.isAuthenticated()) {
            return "redirect:" + HOME_URL;
        }
        model.addAttribute("account", new AccountRequest());
        return REGISTER_TEMPLATE;
    }

    @PostMapping(value = SecurityConstants.REGISTER_URL)
    @ResponseBody
    public UserResponse createAccount(@RequestBody AccountRequest account) {
        return loginService.createAccount(account);
    }
}
