package com.bap.checkin.ui.control.user;

import com.bap.checkin.ui.common.request.AccountRequest;
import com.bap.checkin.ui.common.request.PasswordRequest;
import com.bap.checkin.ui.common.response.*;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@AllArgsConstructor
@PreAuthorize("hasAnyAuthority('Admin', 'Employee')")
public class AccountController {

    private final AccountService accountService;

    /**
     * URL
     */
    private static final String ACCOUNT_INFO_URL = "/account";
    private static final String UPDATE_ACCOUNT_URL = "/account/update";
    private static final String CHANGE_PASSWORD_ACCOUNT_URL = "/account/changePassword";
    private static final String GET_USER_NAME_URL = "/name";
    private static final String CHECKIN_URL = "/checkinRequest";

    /**
     * Template
     */
    private static final String UPDATE_TEMPLATE = "account/info";
    private static final String CHANGE_PASSWORD_TEMPLATE = "account/change_password";

    @PreAuthorize("hasAnyAuthority('Admin', 'Employee')")
    @GetMapping(value = ACCOUNT_INFO_URL)
    public String getEditInfoPage(Model model) {
        model.addAttribute("user", new UserResponse());
        return UPDATE_TEMPLATE;
    }

    @PostMapping(value = ACCOUNT_INFO_URL)
    @ResponseBody
    public AccountInfoResponse getInfo(Principal principal) {
        String email = principal.getName();
        return accountService.getAccountInfo(email);
    }

    @PostMapping(value = UPDATE_ACCOUNT_URL)
    @ResponseBody
    public MessageResponse updateInfo(@RequestBody AccountRequest account,
                                      Principal principal) {
        String email = principal.getName();
        return accountService.editAccount(email, account);
    }

    @GetMapping(value = CHANGE_PASSWORD_ACCOUNT_URL)
    public String getChangePasswordPage() {
        return CHANGE_PASSWORD_TEMPLATE;
    }


    @PostMapping(value = CHANGE_PASSWORD_ACCOUNT_URL)
    @ResponseBody
    public MessageResponse changePassword(@RequestBody PasswordRequest passwordRequest,
                                          Principal principal) {
        String email = principal.getName();
        return accountService.editPassword(email, passwordRequest);
    }

    @PostMapping(value = GET_USER_NAME_URL)
    @ResponseBody
    public LoginResponse getName(Authentication authentication) {
        List<String> listAuthority = new ArrayList<>();
        for (GrantedAuthority grantedAuthority: authentication.getAuthorities()) {
            listAuthority.add(grantedAuthority.getAuthority());
        }
        return new LoginResponse(authentication.getName(), listAuthority);
    }

    @GetMapping(value = CHECKIN_URL)
    @ResponseBody
    public CheckinInfoResponse getCheckinInfoToday(Principal principal) {
        return accountService.checkinInfoToday(principal.getName());
    }

    @PostMapping(value = CHECKIN_URL)
    @ResponseBody
    public CheckinInfoResponse checkin(Principal principal) {
        return accountService.requestCheckin(principal.getName());
    }
}
