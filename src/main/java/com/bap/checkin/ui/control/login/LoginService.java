package com.bap.checkin.ui.control.login;

import com.bap.checkin.ui.common.UserEnum;
import com.bap.checkin.ui.common.model.User;
import com.bap.checkin.ui.common.repository.UserRepository;
import com.bap.checkin.ui.common.request.AccountRequest;
import com.bap.checkin.ui.common.request.UserRequest;
import com.bap.checkin.ui.common.response.UserResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class LoginService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserResponse login(UserRequest userRequest) {
        UserResponse userResponse = new UserResponse();
        userResponse.setUserName("test");
        Optional<User> user = userRepository
                .findByEmailAndEncryptedPassword(userRequest.getEmail(), passwordEncoder.encode(userRequest.getPassword()));
        return user.map(UserResponse::new).orElse(userResponse);
    }

    public UserResponse createAccount(AccountRequest accountRequest) {
        Optional<User> optionalUser = userRepository.findFirstByEmail(accountRequest.getEmail());
        if (optionalUser.isPresent()) {
            return null;
        }
        User user = accountRequest.getCreatedAccountInfo();
        user.setEncryptedPassword(passwordEncoder.encode(accountRequest.getPassword()));
        user.setStatus(UserEnum.UserStatus.NOT_WORKING.getCode());
        user.setRole(UserEnum.UserRole.EMPLOYEE_ROLE.getCode());
        return (new UserResponse(userRepository.save(user)));
    }
}
