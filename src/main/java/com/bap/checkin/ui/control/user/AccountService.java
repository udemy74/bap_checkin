package com.bap.checkin.ui.control.user;

import com.bap.checkin.ui.common.Constant;
import com.bap.checkin.ui.common.model.Checkin;
import com.bap.checkin.ui.common.model.User;
import com.bap.checkin.ui.common.repository.CheckinRepository;
import com.bap.checkin.ui.common.repository.UserRepository;
import com.bap.checkin.ui.common.request.AccountRequest;
import com.bap.checkin.ui.common.request.PasswordRequest;
import com.bap.checkin.ui.common.response.AccountInfoResponse;
import com.bap.checkin.ui.common.response.CheckinInfoResponse;
import com.bap.checkin.ui.common.response.MessageResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final CheckinRepository checkinRepository;

    public AccountInfoResponse getAccountInfo(String email) {
        Optional<User> optionalUser = userRepository.findFirstByEmail(email);
        if (optionalUser.isEmpty()) {
            return null;
        }
        return new AccountInfoResponse(optionalUser.get());
    }

    public MessageResponse editAccount(String email, AccountRequest accountRequest) {
        Optional<User> optionalUser = userRepository.findFirstByEmail(email);
        if (optionalUser.isEmpty()) {
            return new MessageResponse(Constant.MSG_ERROR_TYPE, Constant.MSG_EDIT_ACCOUNT_FAIL);
        }

        User user = optionalUser.get();
        accountRequest.getAccountInfo(user);
        userRepository.save(user);

        return new MessageResponse(Constant.MSG_SUCCESS_TYPE, Constant.MSG_EDIT_ACCOUNT_SUCCESS);
    }

    public MessageResponse editPassword(String email, PasswordRequest passwordRequest) {
        Optional<User> optionalUser = userRepository.findFirstByEmail(email);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (passwordEncoder.matches(passwordRequest.getOldPassword(), user.getEncryptedPassword())) {
                String newPasswordEncrypted = passwordEncoder.encode(passwordRequest.getNewPassword());
                user.setEncryptedPassword(newPasswordEncrypted);
                userRepository.save(user);
                return new MessageResponse(Constant.MSG_SUCCESS_TYPE, Constant.MSG_CHANGE_PASSWORD_SUCCESS);
            } else {
                return new MessageResponse(Constant.MSG_ERROR_TYPE, Constant.MSG_OLD_PASSWORD_WRONG);
            }
        }

        return new MessageResponse(Constant.MSG_ERROR_TYPE, Constant.MSG_CHANGE_PASSWORD_FAIL);
    }

    public CheckinInfoResponse checkinInfoToday(String email) {
        Optional<User> userOptional = userRepository.findFirstByEmail(email);
        return userOptional.map(user -> new CheckinInfoResponse(getCheckinByUser(user, LocalDate.now()))).orElse(null);

    }

    public CheckinInfoResponse requestCheckin(String email) {
        Optional<User> userOptional = userRepository.findFirstByEmail(email);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            LocalDate currentDate = LocalDate.now();
            LocalDateTime currentTime = LocalDateTime.now();

            Checkin checkin = getCheckinByUser(user, currentDate);
            MessageResponse msg;
            if (checkin.getCheckinTime() != null) {
                checkin.setCheckoutTime(currentTime);
                msg = new MessageResponse(Constant.MSG_SUCCESS_TYPE, Constant.MSG_CHECKOUT_SUCCESS);
            } else {
                checkin.setCheckinTime(currentTime);
                msg = new MessageResponse(Constant.MSG_SUCCESS_TYPE, Constant.MSG_CHECKIN_SUCCESS);
            }
            return new CheckinInfoResponse(checkinRepository.save(checkin), msg);
        }

        return new CheckinInfoResponse();
    }

    public Checkin getCheckinByUser(User user, LocalDate dateCheckin) {
        Optional<Checkin> optionalCheckin = checkinRepository.getFirstByCheckinDateAndUser(dateCheckin, user);
        Checkin checkin;
        if (optionalCheckin.isPresent()) {
            checkin = optionalCheckin.get();
        } else {
            checkin = new Checkin();
            checkin.setUser(user);
            checkin.setCheckinDate(dateCheckin);
        }

        return checkin;
    }
}
