package com.bap.checkin.ui.common;

import java.time.format.DateTimeFormatter;

public class ConstantFormat {
    public static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(Constant.DATE_FORMAT);
    public static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(Constant.DATE_TIME_FORMAT);
}
