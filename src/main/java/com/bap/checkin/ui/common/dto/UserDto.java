package com.bap.checkin.ui.common.dto;

import com.bap.checkin.ui.common.model.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class UserDto implements Serializable {

    private static final long serialVersionUID = 392142L;

    protected String userId;
    private String userName;
    private String password;
    private Integer role;
    private String name;
    private LocalDate birthday;
    private String phoneNumber;
    private String email;

    public UserDto(User user){
        this.userId  = user.getUserId();
        this.userName = user.getUserName();
        this.role = user.getRole();
        this.name = user.getName();
        this.birthday = user.getBirthday();
        this.phoneNumber = user.getPhoneNumber();
        this.email = user.getEmail();
    }
}
