package com.bap.checkin.ui.common;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String MSG_ERROR_TYPE = "error";
    public static final String MSG_SUCCESS_TYPE = "success";

    // Edit account message
    public static final String MSG_EDIT_ACCOUNT_SUCCESS = "Edit Info Success!";
    public static final String MSG_EDIT_ACCOUNT_FAIL = "Edit Info Fail!";

    // Change password message
    public static final String MSG_CHANGE_PASSWORD_SUCCESS = "Change Password Success!";
    public static final String MSG_CHANGE_PASSWORD_FAIL = "Change Password Fail!";
    public static final String MSG_OLD_PASSWORD_WRONG = "Old Password Is Wrong!!";

    // Checkin message
    public static final String MSG_CHECKIN_SUCCESS = "Checkin success!";
    public static final String MSG_CHECKOUT_SUCCESS = "Checkout success!";
    public static final String MSG_CHECKIN_FAIL = "Request fail";

    // Format
    public static final String DATE_FORMAT = "dd/MM/yyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyy HH:mm:ss";
}
