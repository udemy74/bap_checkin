package com.bap.checkin.ui.common.repository;

import com.bap.checkin.ui.common.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findFirstByEmail(String email);

    Optional<User> findByEmailAndEncryptedPassword(String email, String encryptedPassword);
}
