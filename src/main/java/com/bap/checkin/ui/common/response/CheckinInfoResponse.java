package com.bap.checkin.ui.common.response;

import com.bap.checkin.ui.common.Constant;
import com.bap.checkin.ui.common.ConstantFormat;
import com.bap.checkin.ui.common.model.Checkin;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class CheckinInfoResponse {
    private String userName;
    private String checkinDate;
    private String checkinTime;
    private String checkoutTime;
    private MessageResponse messageResponse;

    public CheckinInfoResponse(Checkin checkin) {
        this.userName = checkin.getUser().getUserName();

        this.checkinDate = checkin.getCheckinDate().format(ConstantFormat.DATE_FORMATTER);

        LocalDateTime checkinTime = checkin.getCheckinTime();
        if (checkinTime != null) {
            this.checkinTime = checkinTime.format(ConstantFormat.DATE_TIME_FORMATTER);
        }

        LocalDateTime checkoutTime = checkin.getCheckoutTime();
        if (checkoutTime != null) {
            this.checkoutTime = checkin.getCheckoutTime().format(ConstantFormat.DATE_TIME_FORMATTER);
        }
    }

    public CheckinInfoResponse(Checkin checkin, MessageResponse messageResponse) {
        this(checkin);
        this.messageResponse = messageResponse;
    }

    public CheckinInfoResponse() {
        this.messageResponse = new MessageResponse(Constant.MSG_ERROR_TYPE, Constant.MSG_CHECKIN_FAIL);
    }
}
