package com.bap.checkin.ui.common.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "checkin")
@Getter
@Setter
public class Checkin {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    @Column(name = "checkin_date")
    private LocalDate checkinDate;

    @Column(name = "checkin_time")
    private LocalDateTime checkinTime;

    @Column(name = "checkout_time")
    private LocalDateTime checkoutTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
