package com.bap.checkin.ui.common.response;

import com.bap.checkin.ui.common.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse implements Serializable {
    private static final long serialVersionUID = 115152L;

    private String id;
    private String userName;
    private Integer role;

    public UserResponse(User user) {
        this.id = user.getUserId();
        this.userName = user.getUserName();
        this.role = user.getRole();
    }
}
