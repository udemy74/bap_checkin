package com.bap.checkin.ui.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class UserEnum {

    @Getter
    @AllArgsConstructor
    public enum UserRole {
        ADMIN_ROLE(1, "Admin"),
        EMPLOYEE_ROLE(2, "Employee");
        Integer code;
        String content;
    }

    @Getter
    @AllArgsConstructor
    public enum UserStatus {
        NOT_WORKING(0, "Not Working"),
        WORKING(1, "Working");
        Integer code;
        String content;
    }
}
