package com.bap.checkin.ui.common.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "user")
@Getter
@Setter
public class User implements Serializable {

    private static final long serialVersionUID = 392142L;

    @Id
    @Column(name = "id")
    @GeneratedValue
    protected Integer id;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "user_name", nullable = false, length = 60)
    private String userName;

    @Column(name = "encrypted_password", nullable = false)
    private String encryptedPassword;

    @Column(name = "status", nullable = false)
    private Integer status = 0;
    @Column(name = "role", nullable = false)
    private Integer role = 0;

    @Column(name = "name", nullable = false, length = 60)
    @Size(max = 255)
    private String name;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "phone_number", length = 20)
    private String phoneNumber;

    @Column(name = "email", nullable = false, length = 100, unique = true)
    private String email;
}
