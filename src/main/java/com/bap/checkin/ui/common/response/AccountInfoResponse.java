package com.bap.checkin.ui.common.response;

import com.bap.checkin.ui.common.model.User;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AccountInfoResponse {
    private String userName;
    private String name;
    private String birthday;
    private String phoneNumber;
    private String email;

    public AccountInfoResponse(User user) {
        this.userName = user.getUserName();
        this.name = user.getName();
        this.birthday = user.getBirthday().toString();
        this.phoneNumber = user.getPhoneNumber();
        this.email = user.getEmail();
    }
}
