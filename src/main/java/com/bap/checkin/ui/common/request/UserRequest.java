package com.bap.checkin.ui.common.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class UserRequest implements Serializable {

    private static final long serialVersionUID = 66221232L;

    private String email;
    private String password;
}
