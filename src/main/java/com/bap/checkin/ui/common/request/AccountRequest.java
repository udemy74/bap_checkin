package com.bap.checkin.ui.common.request;

import com.bap.checkin.ui.common.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class AccountRequest {
    private String userName;
    private String name;
    private String password;
    private LocalDate birthday;
    private String phoneNumber;
    private String email;

    public void getAccountInfo(User user) {
        user.setUserName(userName);
        user.setBirthday(birthday);
        user.setPhoneNumber(phoneNumber);
        user.setName(name);
        user.setUserId(userName);
    }

    public User getCreatedAccountInfo() {
        User user = new User();
        user.setEmail(email);
        this.getAccountInfo(user);
        return user;
    }
}
