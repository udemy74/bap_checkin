package com.bap.checkin.ui.common.repository;

import com.bap.checkin.ui.common.model.Checkin;
import com.bap.checkin.ui.common.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface CheckinRepository extends CrudRepository<Checkin, Integer> {
    Optional<Checkin> getFirstByCheckinDateAndUser(LocalDate checkinDate, User user);
}
