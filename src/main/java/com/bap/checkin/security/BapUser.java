package com.bap.checkin.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.List;

@Getter
@Setter
public class BapUser extends User {
    private String name;

    public BapUser(String name, String email, String password, List<GrantedAuthority> authorities) {
        super(email, password, authorities);
        this.name = name;
    }
}
