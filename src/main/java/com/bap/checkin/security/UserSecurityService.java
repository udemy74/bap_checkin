package com.bap.checkin.security;

import com.bap.checkin.ui.common.UserEnum;
import com.bap.checkin.ui.common.model.User;
import com.bap.checkin.ui.common.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Value("${checkin.security.secret_token")
    private String secretToken;

    @Value("${checkin.security.expiration_time}")
    private Long expirationTime;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = getUser(email);
        List<GrantedAuthority> grantedAuthorityDefaults = this.getListAuthorities(user);

        return new BapUser(user.getName(), user.getEmail(), user.getEncryptedPassword(), grantedAuthorityDefaults);
    }

    public User getUser(String email) {
        Optional<User> userOptional = userRepository.findFirstByEmail(email);
        if (userOptional.isEmpty()) {
            log.error("Can't find " + email + " in database");
            throw new UsernameNotFoundException(email + " was not found");
        }

        User user = userOptional.get();

        if (user.getStatus().equals(UserEnum.UserStatus.NOT_WORKING.getCode())) {
            log.error("User " + email + " not working");
            throw new UsernameNotFoundException(email + " not working");
        }

        return user;
    }

    public List<GrantedAuthority> getListAuthorities(User user) {
        List<GrantedAuthority> grantedAuthorityDefaults = new ArrayList<>();
        if (user.getRole().equals(UserEnum.UserRole.ADMIN_ROLE.getCode())) {
            String role = UserEnum.UserRole.ADMIN_ROLE.getContent();
            grantedAuthorityDefaults.add(new SimpleGrantedAuthority(role));
        }
        if (user.getRole() >= UserEnum.UserRole.EMPLOYEE_ROLE.getCode()) {
            String role = UserEnum.UserRole.EMPLOYEE_ROLE.getContent();
            grantedAuthorityDefaults.add(new SimpleGrantedAuthority(role));
        }
        return grantedAuthorityDefaults;
    }

    public String getTokenValue(String token) {
        return Jwts.parser()
                .setSigningKey(secretToken)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    public String createToken(String value) {
        return Jwts.builder()
                .setSubject(value)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .signWith(SignatureAlgorithm.HS512, secretToken)
                .compact();
    }
}
