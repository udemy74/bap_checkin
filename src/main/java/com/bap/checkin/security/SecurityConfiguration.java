package com.bap.checkin.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {
    @Autowired
    private UserSecurityService userSecurityService;

    @Autowired
    private AuthenticationConfiguration configuration;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userSecurityService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(SecurityConstants.LOGIN_PAGE_URL
                        , SecurityConstants.LOGIN_PROCESSING_URL
                        , SecurityConstants.REGISTER_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(getAuthenticationFilter())
                .addFilter(getAuthorizationFilter())
                .formLogin()
                .loginPage(SecurityConstants.LOGIN_PAGE_URL)
                .loginProcessingUrl(SecurityConstants.LOGIN_PROCESSING_URL)
                .failureHandler(getLoginFailureHandler())
                .permitAll()
                .and()
                .logout()
                .logoutUrl(SecurityConstants.LOGOUT_PROCESSING_URL)
                .deleteCookies("JSESSIONID")
                .and()
                .exceptionHandling()
                .accessDeniedPage("/403");

        http.sessionManagement().invalidSessionUrl("/login?expired=true");

        return http.build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().antMatchers(HttpMethod.GET, "/static/layout/**");
    }

    @Bean
    public LoginFailureHandler getLoginFailureHandler() {
        return new LoginFailureHandler();
    }

    protected AuthenticationFilter getAuthenticationFilter() throws Exception {
        final AuthenticationFilter filter = new AuthenticationFilter(configuration.getAuthenticationManager(), userSecurityService);
        filter.setFilterProcessesUrl(SecurityConstants.LOGIN_PROCESSING_URL);
        return filter;
    }

    protected AuthorizationFilter getAuthorizationFilter() throws Exception {
        return new AuthorizationFilter(configuration.getAuthenticationManager(), userSecurityService);
    }
}
