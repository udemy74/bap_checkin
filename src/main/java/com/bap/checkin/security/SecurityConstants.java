package com.bap.checkin.security;


public class SecurityConstants {
    public static final long PASSWORD_RESET_EXPIRATION_TIME = 3600000; // 1 hour
    public static final String HEADER_STRING = "Authorization";
    public static final String LOGIN_PAGE_URL = "/login";
    public static final String LOGIN_PROCESSING_URL = "/users/login";
    public static final String LOGOUT_PROCESSING_URL = "/logout";
    public static final String REGISTER_URL = "/register";
}
