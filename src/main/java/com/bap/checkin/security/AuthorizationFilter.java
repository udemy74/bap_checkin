package com.bap.checkin.security;

import com.bap.checkin.ui.common.model.User;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    private UserSecurityService userSecurityService;

    public AuthorizationFilter(AuthenticationManager authManager, UserSecurityService userSecurityService) {
        super(authManager);
        this.userSecurityService = userSecurityService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {

        String token = getToken(req);

        if (token == null) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        String email = userSecurityService.getTokenValue(token);

        if (email != null) {
            User user = userSecurityService.getUser(email);
            return new UsernamePasswordAuthenticationToken(email, null, userSecurityService.getListAuthorities(user));
        }

        return null;
    }

    private String getToken(HttpServletRequest request) {
        String token = request.getHeader(SecurityConstants.HEADER_STRING);

        if (token == null) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(SecurityConstants.HEADER_STRING)) {
                    token = cookie.getValue();
                    break;
                }
            }
        }

        return token;
    }
}
 